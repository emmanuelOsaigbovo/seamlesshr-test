
## SEAMLESS-HR INTERVIEW TEST

- Created an endpoint for User login and registration. Implemented JWT.

- ##### ENDPOINT 
- POST /auth/register
- POST /auth/login

- Added 3 more self-defined columns to the course table, with another migration (add_self_defined_columns_to_courses).

- Created an endpoint to call a course factory to create 50 courses. Queued this operation in a job.

- ##### ENDPOINT 
- POST /courses


- Created an endpoint for a user to register in one or more courses. 
- ##### ENDPOINT 
- POST /users/courses

- Created an endpoint to see list of all courses. If the user is registered in a course,  the date enrolled in that course is returned.
- ##### ENDPOINT 
- GET /courses

- Created an endpoint to export all courses with the Maatexcel (^3.1) package in excel or csv format.
- ##### ENDPOINT 
- POST /courses/export


## SOLID Principles

- **Single Responsibility Principle**
- **Open Close Principle**
- **Interface Segregation Principle**
- **Liskov Substitution Principle**
- **Dependency Inversion**

## DESIGN PATTERNS USED

- **Repository Pattern**
- **Provider Patttern**
- **Strategy Pattern**
- **Factory Pattern**

## Installation

1. Clone this repository: `git clone https://bitbucket.org/emmanuelOsaigbovo/seamlesshr-test.git`
2. Run `cd seamlesshr-test && composer install`
3. Run `cp .env.example .env`
4. Run `php artisan key:generate`
4. Set up database, queue driver, export_format in .env
5. Run `php artisan migrate`
6. Run `php artisan db:seed`
7. Run `php artisan serve`
8. Run `php artisan queue:work`
9. Run Tests `vendor/bin/phpunit`

##POSTMAN COLLECTION LINK 
- (https://www.getpostman.com/collections/60ac3f45002304b9cdb0)

The request and response formats and headers adheres to [JSON API](http://jsonapi.org) 

