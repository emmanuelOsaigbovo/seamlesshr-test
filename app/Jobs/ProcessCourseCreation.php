<?php

namespace App\Jobs;

use App\Course;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessCourseCreation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /**
         * Creates 50 Courses, and 50 users, registering each
         * one of the created user to a course.
         */
        return factory(\App\Course::class, 50)
            ->create()
            ->each(function ($course) {
                $course->users()->sync(factory(\App\User::class)->create()->id);
            });

        /**
         * creates 50 courses
         */
        // return factory(Course::class, 50)->create();
    }
}
