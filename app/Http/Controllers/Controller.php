<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * returns bearer Token for user.
     * @param string $token
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondWithToken(string $token)
    {
        return response()->json([
            'data' => [
                'token_type' => 'bearer',
                'expires_in' => auth()->factory()->getTTL() * 60,
                'access_token' => $token,
            ]
        ], Response::HTTP_CREATED);
    }

    /**
     * returns response that adheres to the 
     * JSON:API Specification.
     * @param string $title
     * @param string $details
     * @return \Illuminate\Http\JsonResponse
     */
    public function jsonAPIResponse(string $title, string $details, ?int $statusCode = Response::HTTP_OK)
    {
        return response()->json([
            "data" => [
                "title" => $title,
                // "details" => $details
            ]
        ], $statusCode);
    }
}
