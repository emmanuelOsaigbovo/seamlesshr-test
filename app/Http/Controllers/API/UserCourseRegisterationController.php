<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CourseRegisterationRequest;
use App\Repositories\Contracts\UserRepositoryInterface;

class UserCourseRegisterationController extends Controller
{
    public function __construct(UserRepositoryInterface $userRepo)
    {
        $this->middleware(['JWT', 'json.api.headers']);
        $this->user = $userRepo;
    }

    /**
     * Registers the user in one or more courses
     * @param \App\Http\Requests\CourseRegisterationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CourseRegisterationRequest $request)
    {
        $this->user->registerCourse();
        return $this->jsonAPIResponse("Courses registered", "");
    }
}
