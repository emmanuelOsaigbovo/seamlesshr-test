<?php

namespace App\Http\Controllers\API;

use App\Jobs\ProcessCourseCreation;
use App\Http\Controllers\Controller;
use App\Http\Resources\CourseResource;
use App\Repositories\Contracts\CourseRepositoryInterface;
use Symfony\Component\HttpFoundation\Response;

class CoursesController extends Controller
{
    public $course;

    public function __construct(CourseRepositoryInterface $courseRepo)
    {
        $this->middleware('json.api.headers');
        $this->middleware('JWT', ['except' => ['createbatch']]);
        $this->course = $courseRepo;
    }

    /**
     * returns all courses
     * @return \Illuminate\Http\Resources\Json\Resource
     */
    public function index()
    {
        $courses = $this->course->getAll();
        return CourseResource::collection($courses);
    }

    /**
     * creates bulk/batch courses
     * @return \Illuminate\Http\JsonResponse
     */
    public function store()
    {
        /**
         * Dispatched bulk creation of courses 
         * since this is a long running process
         * it's best to run it in the background using jobs/queues
         */
        ProcessCourseCreation::dispatch();
        return $this->jsonAPIResponse("batch courses created, total record 50", "",  Response::HTTP_CREATED);
    }
}
