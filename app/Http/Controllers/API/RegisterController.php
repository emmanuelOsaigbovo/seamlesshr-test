<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Repositories\Contracts\UserRepositoryInterface;

class RegisterController extends Controller
{
    public $user;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->user = $userRepository;
    }

    /**
     * Registers the user, and returns the  user's bearerToken
     * @param \App\Http\Requests\CreateUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateUserRequest $request)
    {
        $user = $this->user->register($request);
        $token = auth()->login($user);
        return $this->respondWithToken($token);
    }
}
