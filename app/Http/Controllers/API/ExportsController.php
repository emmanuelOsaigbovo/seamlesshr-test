<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Strategies\Exports\ExportStrategy;

class ExportsController extends Controller
{
    public function __construct(ExportStrategy $exportStrategy)
    {
        $this->middleware(['JWT', 'json.api.headers']);
        $this->exportStrategy = $exportStrategy;
    }

    /**
     * Exports all courses in excel or csv format
     * @return \Illuminate\Http\JsonResponse
     */
    public function export()
    {
        $this->exportStrategy->export();
        return $this->jsonAPIResponse("Courses exported successfully", "");
    }
}
