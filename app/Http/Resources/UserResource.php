<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "date_enrolled" => $this->whenPivotLoaded('course_user', function () {
                return $this->when(auth()->id() == $this->id, function () {
                    return $this->pivot->created_at->toDateString();
                });
            })
        ];
    }
}
