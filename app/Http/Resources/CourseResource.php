<?php

namespace App\Http\Resources;

use App\Http\Resources\UserResource;
use App\Http\Resources\UsersIdentifierResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CourseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string) $this->id,
            'type' => 'courses',
            'attributes' => [
                'name' => $this->name,
                'description' => $this->description,
                'difficulty' => $this->difficulty,
                'created_at' => $this->created_at->diffForHumans(),
            ],
            'relationships' => [
                'users' => [
                    'data' => UserResource::collection(
                        $this->whenLoaded('users')
                    ),
                ],
            ],
        ];
    }
}
