<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['users'];


    /**
     * Get the users registered for a course.
     */
    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('created_at', 'updated_at')->withTimestamps();
    }
}
