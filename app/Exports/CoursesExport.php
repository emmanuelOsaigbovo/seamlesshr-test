<?php

namespace App\Exports;

use App\Course;
use Maatwebsite\Excel\Concerns\FromQuery;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use App\Repositories\Contracts\CourseRepositoryInterface;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class CoursesExport implements FromQuery, WithMapping, WithHeadings, WithColumnFormatting
{
    public function __construct(CourseRepositoryInterface $courseRepo)
    {
        $this->course = $courseRepo;
    }

    public function map($course): array
    {
        return [
            $course->name,
            $course->description,
            $course->difficulty,
            Date::dateTimeToExcel($course->created_at),
        ];
    }

    public function columnFormats(): array
    {
        return [
            'D' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }

    public function headings(): array
    {
        return [
            'Name',
            'Description',
            'Difficulty',
            'Created At',
        ];
    }


    /**
     * @return 
     */
    public function query()
    {
        // dd($this->course->getSpecificFields(['name', 'description', 'difficulty', 'created_at']));
        return $this->course->getSpecificFields(['name', 'description', 'difficulty', 'created_at']);
    }
}
