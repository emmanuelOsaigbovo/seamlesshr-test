<?php

namespace App\Strategies\Exports;

use App\Repositories\Concretes\EloquentCourseRepository;
use App\Repositories\Contracts\CourseRepositoryInterface;
use Maatwebsite\Excel\Exporter;

class ExportFactory
{
    public $course;

    public function __construct()
    {
        $this->course = new EloquentCourseRepository;
    }
    /**
     * stores the exported csv formatted file on disk
     * @param string $format
     * @return  \App\Strategies\Exports\CSVExport|\App\Strategies\Exports\ExcelExport
     */
    public function make(string $format)
    {
        /**
         * If the format for the courses to be exported is
         * not given, then it defaults to the format set in
         * config file / ENV file
         */

        /**
         * Export format.
         *
         * @var string
         */
        $format  = $format ?? config('exports.format');

        switch ($format) {
            case "csv":
                return new CSVExport($this->course);
            case 'excel':
                return new ExcelExport($this->course);
            default:
                return new ExcelExport($this->course);
        }
    }
}
