<?php

namespace App\Strategies\Exports;

/**
 * contract for Exports
 */
interface ExportStrategy
{
    public function export();
}
