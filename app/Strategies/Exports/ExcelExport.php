<?php

namespace App\Strategies\Exports;

use App\Traits\Filenameable;
use App\Exports\CoursesExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Repositories\Contracts\CourseRepositoryInterface;

class ExcelExport implements ExportStrategy
{
    use Filenameable;

    /**
     * Filename.
     *
     * @var string
     */

    public $fileName;

    const EXT = ".xlsx";


    public function __construct(CourseRepositoryInterface $course)
    {
        $this->fileName = $this->getFileName();
        $this->course = $course;
    }

    /**
     * stores the exported excel formatted file on disk
     * @return bool 
     */
    public function export()
    {
        Excel::store(new CoursesExport($this->course), $this->fileName);
        return true;
    }
}
