<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Array of Contracts and Concretes Repositories.
     *
     * @var array
     */
    protected $repos = [
        
    ];

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Contracts\UserRepositoryInterface', 'App\Repositories\Concretes\EloquentUserRepository');
        $this->app->bind('App\Repositories\Contracts\CourseRepositoryInterface', 'App\Repositories\Concretes\EloquentCourseRepository');
    }
}
