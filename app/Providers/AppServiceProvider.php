<?php

namespace App\Providers;

use App\Strategies\Exports\ExportFactory;
use App\Strategies\Exports\ExportStrategy;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RepositoryServiceProvider::class);
        $this->app->singleton(ExportStrategy::class, function ($app) {
            $factory = new ExportFactory;
            return $factory->make(request()->input('data.attributes.format'));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
