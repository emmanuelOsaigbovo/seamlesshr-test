<?php

namespace App\Repositories\Concretes;

use App\User;
use App\Course;
use Illuminate\Http\Request;
use App\Repositories\Contracts\CourseRepositoryInterface;

class EloquentCourseRepository implements CourseRepositoryInterface
{
    /**
     * Creates a new course resource
     * 
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\User
     */
    public function register(Request $request)
    {
        $coursesIds = request()->input("data.attributes");
        return auth()->user()->courses()->createMany($coursesIds);
    }

    /**
     * Fetchs all courses
     * @param void
     * @return \App\Course
     */
    public function getAll()
    {
        return Course::all();
    }

    /**
     * Fetchs a specific fields for courses
     * @param string $fields
     * @return \App\Course
     */
    public function getSpecificFields($fields)
    {
        return Course::select(...$fields);
    }
}
