<?php

namespace App\Repositories\Concretes;

use App\User;
use Illuminate\Http\Request;
use App\Repositories\Contracts\UserRepositoryInterface;

class EloquentUserRepository implements UserRepositoryInterface
{
    /**
     * Creates a new user resource
     * 
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\User
     */
    public function register(Request $request)
    {
        return User::create(request()->input('data.attributes'));
    }

    /**
     * Registers a user in a course.
     * @param void
     */
    public function registerCourse()
    {
        $user = auth()->user();
        $courseId =  request()->input('data.attributes.courseId');
        return $user->courses()->attach($courseId);
    }
}
