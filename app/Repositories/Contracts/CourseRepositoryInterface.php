<?php

namespace App\Repositories\Contracts;

use Illuminate\Http\Request;

interface CourseRepositoryInterface

{
    public function register(Request $request);

    public function getAll();

    public function getSpecificFields($fields);
}
