<?php

namespace App\Traits;

trait Filenameable
{
   /**
    * generates a random name for the exported excel/csv file
    * @return string 
    */
    public function getFileName()
    {
        $fileNameInput = request()->input('data.attributes.filename');
        return $this->fileName = $fileNameInput ? $fileNameInput. self::EXT : random_string(20) . 'courses' . self::EXT;
    }
}
