<?php

namespace Tests;

use App\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function generateRegisteredUserAndRespectiveBearerToken()
    {
        $user = factory(User::class)->create([
            "email" => "emmanuel@gmail.com",
            "password" => "secret"
        ]);
        $token =  auth()->tokenById($user->id);
        return [$user, $token];
    }
}
