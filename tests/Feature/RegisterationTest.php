<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;

class RegisterationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_register()
    {
        $uri = route("user.register");

        $data = [
            "data" => [
                "type" => "users",
                "attributes" => [
                    "name" => "Emmanuel",
                    "email" => "Osaigbovoemmanuel1@gmail.com",
                    "password" => "secret"
                ]
            ]
        ];

        $headers = [
            "accept" => "application/vnd.api+json",
            "content-type" => "application/vnd.api+json"
        ];

        $this->postJson($uri, $data, $headers)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure([
                "data" => [
                    'access_token',
                    'expires_in',
                    'token_type'
                ]
            ]);

        $this->assertDatabaseHas('users', [
            "name" => $data['data']['attributes']["name"],
            "email" => $data['data']['attributes']["email"]
        ]);
    }

    /** @test */
    public function it_validates_that_the_name_is_given_when_registering()
    {
        $data = [
            "data" => [
                "type" => "users",
                "attributes" => [
                    "name" => "",
                    "email" => "Osaigbovoemmanuel1@gmail.com",
                    "password" => "secret"
                ]
            ]
        ];


        $uri = route("user.register");

        $headers = [
            "accept" => "application/vnd.api+json",
            "content-type" => "application/vnd.api+json"
        ];

        $this->postJson($uri, $data, $headers)
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    [
                        'title' => 'Validation Error',
                        'details' => 'The data.attributes.name field is required.',
                        'source' => [
                            'pointer' => '/data/attributes/name',
                        ]
                    ]
                ]
            ]);
    }

    /** @test */
    public function it_validates_that_the_email_is_given_when_registering()
    {
        $uri = route("user.register");
        
        $data = [
            "data" => [
                "type" => "users",
                "attributes" => [
                    "name" => "Emmanuel",
                    "email" => "",
                    "password" => "secret"
                ]
            ]
        ];

        $headers = [
            "accept" => "application/vnd.api+json",
            "content-type" => "application/vnd.api+json"
        ];



        $this->postJson($uri, $data, $headers)
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    [
                        'title' => 'Validation Error',
                        'details' => 'The data.attributes.email field is required.',
                        'source' => [
                            'pointer' => '/data/attributes/email',
                        ]
                    ]
                ]
            ]);
    }

    /** @test */
    public function it_validates_that_the_password_is_given_when_registering()
    {
        $uri = route("user.register");
        
        $data = [
            "data" => [
                "type" => "users",
                "attributes" => [
                    "name" => "Emmanuel",
                    "email" => "Osaigbovoemmanuel1@gmail.com",
                    "password" => ""
                ]
            ]
        ];

        $headers = [
            "accept" => "application/vnd.api+json",
            "content-type" => "application/vnd.api+json"
        ];


        $this->postJson($uri, $data, $headers)
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    [
                        'title' => 'Validation Error',
                        'details' => 'The data.attributes.password field is required.',
                        'source' => [
                            'pointer' => '/data/attributes/password',
                        ]
                    ]
                ]
            ]);
    }
}
