<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_login()
    {
        [$registeredUser, $bearerToken] = $this->generateRegisteredUserAndRespectiveBearerToken();

        $uri = route("user.login");

        $headers = [
            "accept" => "application/vnd.api+json",
            "content-type" => "application/vnd.api+json"
        ];

        $data = [
            "data" => [
                "type" => "users",
                "attributes" => [
                    "email" => $registeredUser->email,
                    "password" => "secret"
                ]
            ]
        ];


        $this->postJson($uri, $data, $headers)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure([
                "data" => [
                    'access_token',
                    'token_type'
                ]
            ]);
    }

    /** @test */
    public function it_validates_that_the_email_is_given_when_logging_in()
    {
        $uri = route("user.login");
        
        $data = [
            "data" => [
                "type" => "users",
                "attributes" => [
                    "email" => "",
                    "password" => "secret"
                ]
            ]
        ];

        $headers = [
            "accept" => "application/vnd.api+json",
            "content-type" => "application/vnd.api+json"
        ];


        $this->postJson($uri, $data, $headers)
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    [
                        'title' => 'Validation Error',
                        'details' => 'The data.attributes.email field is required.',
                        'source' => [
                            'pointer' => '/data/attributes/email',
                        ]
                    ]
                ]
            ]);
    }

    /** @test */
    public function it_validates_that_the_password_is_given_when_logging_in()
    {
        $uri = route("user.login");

        $data = [
            "data" => [
                "type" => "users",
                "attributes" => [
                    "email" => "emmanuel@gmail.com",
                    "password" => ""
                ]
            ]
        ];

        $headers = [
            "accept" => "application/vnd.api+json",
            "content-type" => "application/vnd.api+json"
        ];

        $this->postJson($uri, $data, $headers)
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    [
                        'title' => 'Validation Error',
                        'details' => 'The data.attributes.password field is required.',
                        'source' => [
                            'pointer' => '/data/attributes/password',
                        ]
                    ]
                ]
            ]);
    }
}
