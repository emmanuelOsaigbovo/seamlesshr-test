<?php

namespace Tests\Feature;

use App\Course;
use App\Exports\CoursesExport;
use Tests\TestCase;
use App\Jobs\ProcessCourseCreation;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Queue;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CourseTest extends TestCase
{
    use RefreshDatabase;
    public $user;
    public $bearerToken;

    public function setUp(): void
    {
        parent::setUp();

        [$this->user, $this->bearerToken] = $this->generateRegisteredUserAndRespectiveBearerToken();
    }

    /** @test */
    public function it_can_create_queued_courses()
    {
        Queue::fake();

        $uri = route('courses.batchcreate');

        $data = [];

        $headers = [
            "accept" => "application/vnd.api+json",
            "content-type" => "application/vnd.api+json",
            'Authorization' => "Bearer {$this->bearerToken}"
        ];

        $this->postJson(
            $uri,
            $data,
            $headers
        )->assertStatus(Response::HTTP_CREATED);

        Queue::assertPushed(ProcessCourseCreation::class);
    }

    /** @test */
    public function it_can_be_exported()
    {
        Excel::fake();

        $uri = route('courses.export');

        $data = [
            "data" => [
                "attributes" => [
                    "format" => "xlsx",
                    "filename" => "test"
                ]
            ]
        ];

        $headers =  [
            "accept" => "application/vnd.api+json",
            "content-type" => "application/vnd.api+json",
            'Authorization' => "Bearer {$this->bearerToken}"
        ];

        $this->postJson($uri, $data, $headers)
            ->assertOk();

        // Excel::assertStored('test.xlsx', 'local');
    }

    /** @test */
    public function it_returns_all_courses()
    {
       
        $courses = factory(Course::class, 2)->create();
        $uri = route('courses.index');

        $this->getJson($uri, [
            "accept" => "application/vnd.api+json",
            'Authorization' => "Bearer {$this->bearerToken}"
        ])->assertStatus(200)->assertJson([
            "data" => [
                [
                    "id" => '1',
                    "type" => "courses",
                    "attributes" => [
                        'name' => $courses[0]->name,
                        'description' => $courses[0]->description,
                        'difficulty' => $courses[0]->difficulty,
                        'created_at' => $courses[0]->created_at->diffForHumans(),
                    ],
                    "relationships" => [
                        "users" => [
                            "data" => []
                        ]
                    ]
                ],
                [
                    "id" => '2',
                    "type" => "courses",
                    "attributes" => [
                        'name' => $courses[1]->name,
                        'description' => $courses[1]->description,
                        'difficulty' => $courses[1]->difficulty,
                        'created_at' => $courses[1]->created_at->diffForHumans(),
                    ],
                    "relationships" => [
                        "users" => [
                            "data" => []
                        ]
                    ]
                ],
            ]
        ]);
    }

    /** @test */
    public function it_returns_all_courses_with_enrollment_date_for_authenticated_user()
    {
        $course = factory(Course::class)->create();

        $this->user->courses()->attach($course->id);

        foreach ($this->user->courses as $course) {
            $enrollmentDate  =  $course->pivot->created_at;
        }

        $uri = route('courses.index');

        $headers = [
            "accept" => "application/vnd.api+json",
            "Authorization" => "Bearer {$this->bearerToken}"
        ];

        $this->getJson($uri, $headers)
            ->assertStatus(200)
            ->assertJson([
                "data" => [
                    [
                        "id" => '1',
                        "type" => "courses",
                        "attributes" => [
                            'name' => $course->name,
                            'description' => $course->description,
                            'difficulty' => $course->difficulty,
                            'created_at' => $course->created_at->diffForHumans(),
                        ],
                        "relationships" => [
                            "users" => [
                                "data" => [
                                    [
                                        "date_enrolled" => $enrollmentDate->toDateString()
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]);
    }
}
