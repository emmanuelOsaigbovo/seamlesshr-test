<?php

namespace Tests\Feature;

use App\User;
use App\Course;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserCourseRegisterationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_register_courses()
    {
        $this->withoutExceptionHandling();

        [$user, $bearerToken] = $this->generateRegisteredUserAndRespectiveBearerToken();
     
        $course = factory(Course::class)->create();

        $data = [
            "data" => [
                "type" => "courses",
                "attributes" => [
                    "courseId" => [
                        $course->id
                    ]
                ]
            ]
        ];

        $headers = [
            "accept" => "application/vnd.api+json",
            "content-type" => "application/vnd.api+json",
            "Authorization" => "Bearer {$bearerToken}"
        ];

        $uri = route('user.courses.register');

        $this->postJson($uri, $data, $headers)
            ->assertOk();

        $this->assertDatabaseHas('course_user', [
            "course_id" => $course->id,
            "user_id" => $user->id
        ]);
    }
}
