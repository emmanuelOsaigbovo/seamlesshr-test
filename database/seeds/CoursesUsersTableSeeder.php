<?php

use Illuminate\Database\Seeder;

class CoursesUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Course::class, 50)
            ->create()
            ->each(function ($course) {
                $course->users()->sync(factory(\App\User::class)->create()->id);
            });
    }
}
