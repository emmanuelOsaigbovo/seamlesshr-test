<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    // $levels = [
    //     "beginner",
    //     "intermediate",
    // ];
    // $randValue =  array_rand($levels);
    return [
        "name" => $faker->name,
        "description" => $faker->paragraph(),
        "difficulty" => "beginner"
    ];
});


