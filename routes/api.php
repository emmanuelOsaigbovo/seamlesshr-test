<?php

Route::group([
    'prefix' => 'v1/auth',
    'namespace' => "API"
], function ($router) {
    Route::post('register', 'RegisterController@store')->name('user.register');
    Route::post('login', 'LoginController@login')->name('user.login');
});

Route::group([
    'prefix' => 'v1',
    'namespace' => "API"
], function ($router) {
    Route::post(
        '/users/courses',
        'UserCourseRegisterationController@store'
    )->name('user.courses.register');
    Route::get('/courses', 'CoursesController@index')->name('courses.index');
    Route::post('/courses/export', 'ExportsController@export')->name('courses.export');
    Route::post('/courses', 'CoursesController@store')->name("courses.batchcreate");
});
